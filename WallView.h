#ifndef __WALLVIEW_H__
#define __WALLVIEW_H__

#include <SANDAL2/SANDAL2.h>

#define MARGE 20

enum {
  WALL_RIGHT = 0,
  WALL_LEFT = 1,
  WALL_FRONT = 2,
  WALL_BACK = 3
};

int QuelMur(Element * Perso);

void AffichePerso(Element * Perso, int valeurMur);

void AfficheBlock(Element * Block, int valeurMur, int * idBloc, int nbBloc);

float * TrieBlock(Element * Perso, int valeurMur);

void ViewWall(Element * Perso);

void EndViewWall(Element * Perso, SDL_Keycode c);

#endif
