#include <SANDAL2/SANDAL2.h>
#include <stdio.h>
#include "perso.h"
#include "donnees.h"
#include "Block.h"

void level1(int * pnum_lvl)
{
    Element * player = initPlayer(250, 480, 62, 74, "./assets/sprite.png");

    //int black[] = {0, 0, 0, 255};

    if(player)
    {
        creerBlock(player, 0, 0, 640, 560, 0, 0, SOL, 0, pnum_lvl);
        creerBlock(player, 0, 560, 480, 80, 0, 0, SOL, 1, pnum_lvl);
        creerBlock(player, 560, 560, 80, 80, 0, 0, SOL, 2, pnum_lvl);
        winBlock(player, 480, 560, 80, 80, 0, 0, 3, pnum_lvl);
        creerBlock(player, 480, 560, 80, 80, -1, -1, SOL, 4, pnum_lvl);

        //createText(50, 100, 500, 100, 100, "assets/arial.ttf", "Le but est de frapper la boule rouge avec le clic gauche", black, 100, JEU_d, -1);
        createImage(40, 40, 560, 460, "assets/tuto.png", JEU_d, -1);
    }
}

void level2(int * pnum_lvl)
{
    Element * player = initPlayer(200, 550, 62, 74, "./assets/sprite.png");
    int black[] = {0, 0, 0, 255};

    if(player)
    {
        creerBlock(player, 0, 0, 128, 128, 0, 0, SOL, 0, pnum_lvl);
        creerBlock(player, 256, 0, 384, 128, 0, 0, SOL, 1, pnum_lvl);
        creerBlock(player, 0, 128, 640, 128, 0, 0, SOL, 2, pnum_lvl);
        creerBlock(player, 0, 256, 640, 128, -1, -1, NORMAL, 3, pnum_lvl);
        creerBlock(player, 0, 384, 640, 128, 1, 0, NORMAL, 4, pnum_lvl);
        creerBlock(player, 0, 512, 640, 128, 0, 0, SOL, 5, pnum_lvl);
        winBlock(player, 128, 0, 128, 128, 0, 0, 6, pnum_lvl);
        creerBlock(player, 128, 0, 128, 128, -1, -1, SOL, 7, pnum_lvl);

        createText(50, 150, 500, 50, 50, "assets/arial.ttf", "Ils sont sur les murs !", black, 100, JEU_d, -1);
        createText(25, 200, 550, 50, 50, "assets/arial.ttf", "Appuyez sur echap pour vous en sortir", black, 100, JEU_d, -1);
    }
}

void level(int * pnum_lvl)
{
  Element * player = initPlayer(100, 100, 62, 74, "./assets/sprite.png");

  if(player)
  {
      //sol
      creerBlock(player, 0, 80, 400, 160, 0, 0, SOL, 0, pnum_lvl);
      creerBlock(player, 480, 0, 160, 80, 0, 0, SOL, 1, pnum_lvl);
      creerBlock(player, 0, 240, 320, 80, 0, 0, SOL, 2, pnum_lvl);
      creerBlock(player, 0, 320, 640, 320, 0, 0, SOL, 3, pnum_lvl);
      creerBlock(player, 0, 0, 80, 80, 0, 0, SOL, 4, pnum_lvl);


      creerBlock(player, 480, 160, 80, 80, 1,  1, FRIABLE, 5, pnum_lvl);

      creerBlock(player, 80, 0, 80, 80, 1, 1, SLIME, 6, pnum_lvl);
      creerBlock(player, 160, 0, 160, 80, 1, 1, FIXE, 7, pnum_lvl);
      creerBlock(player, 320, 0, 80, 80, 0, 0, NORMAL, 8, pnum_lvl);
      creerBlock(player, 400, 0, 80, 80, 1, 1, FIXE, 9, pnum_lvl);
      creerBlock(player, 400, 80, 80, 80, 2, 0, NORMAL, 10, pnum_lvl);
      creerBlock(player, 400, 160, 80, 80, 1, 1, FIXE, 11, pnum_lvl);
      creerBlock(player, 480, 80, 160, 80, 1, 0, FIXE, 12,pnum_lvl);
      creerBlock(player, 560, 160, 80, 80, 2, 2, SLIME, 13, pnum_lvl);
      creerBlock(player, 400, 240, 80, 80, 0, 0, NORMAL, 14, pnum_lvl);
      creerBlock(player, 480, 240, 160, 80, 2, 0, NORMAL, 15, pnum_lvl);
      winBlock(player, 320, 240, 80, 80, 2, 2, 16,pnum_lvl);
      creerBlock(player, 320, 240, 80, 80, -1, -1, SOL, 17, pnum_lvl);


  }
}

void level4(int * pnum_lvl)
{
    Element * player = initPlayer(5, 5, 62, 74, "./assets/sprite.png");

    if(player)
    {
        creerBlock(player, 0, 0, 160, 160, 0, 0, SOL, 0, pnum_lvl);
         creerBlock(player, 80, 0, 80, 80, 2, 2, FIXE, 1, pnum_lvl);
        creerBlock(player, 0, 80, 160, 80, 2, 2, FIXE, 2, pnum_lvl);
        creerBlock(player, 160, 0, 160, 80, 1, 0, FIXE, 3, pnum_lvl);
        creerBlock(player, 160, 80, 80, 80, 1, 0, SLIME, 4, pnum_lvl);
        creerBlock(player, 160, 160, 80, 80, 1, 0, FIXE, 5, pnum_lvl);
        creerBlock(player, 80, 160, 80, 80, 1, 0, SLIME, 6, pnum_lvl);
        creerBlock(player, 0, 160, 80, 160, 1, 0, FIXE, 7, pnum_lvl);
        creerBlock(player, 240, 80, 80, 160, 2, 0, NORMAL, 8, pnum_lvl);
        creerBlock(player, 80, 240, 160, 80, 2, 0, NORMAL, 9, pnum_lvl);
        creerBlock(player, 240, 0, 80, 240, 3, 3, FIXE, 10, pnum_lvl);
        creerBlock(player, 0, 240, 240, 80, 3, 3, FIXE, 11, pnum_lvl);
        creerBlock(player, 240, 240, 240, 240, 2, 0, FIXE, 12, pnum_lvl);
        creerBlock(player, 320, 0, 80, 80, 2, 0, FIXE, 13, pnum_lvl);
        creerBlock(player, 320, 80, 80, 160, 2, 0, SLIME, 14, pnum_lvl);
        creerBlock(player, 80, 320, 160, 80, 2, 0, SLIME, 15, pnum_lvl);
        creerBlock(player, 0, 320, 80, 80, 2, 0, FIXE, 16, pnum_lvl);
        creerBlock(player, 320, 240, 80, 160, 4, 4, FIXE, 17, pnum_lvl);
        creerBlock(player, 240, 320, 80, 80, 4, 4, FIXE, 18, pnum_lvl);
        creerBlock(player, 0, 400, 240, 80, 4, 3, FIXE, 19, pnum_lvl);
        creerBlock(player, 400, 0, 80, 240, 4, 3, FIXE, 20, pnum_lvl);
        creerBlock(player, 240, 400, 160, 80, 3, 0, SLIME, 21, pnum_lvl);
        creerBlock(player, 400, 240, 80, 160, 3, 0, SLIME, 22, pnum_lvl);
        creerBlock(player, 240, 480, 240, 80, 4, 4, FIXE, 23, pnum_lvl);
        creerBlock(player, 480, 240, 80, 240, 4, 4, FIXE, 24, pnum_lvl);
        winBlock(player, 400, 400, 80, 80, 3, 3, 25, pnum_lvl);
    }
}

void levelx(int * pnum_lvl)
{
	Element * player = initPlayer(213,540, 62, 74, "./assets/sprite.png");

	if(player)
	{

		creerBlock(player,0,0,107,107,2,2,FIXE,0,pnum_lvl);
    creerBlock(player,0,0,107,107,0,0,SOL,38,pnum_lvl);
		creerBlock(player,0,107,107,106,0,0,FIXE,2,pnum_lvl);
		creerBlock(player,0,213,107,107,3,3,FIXE,3,pnum_lvl);
		creerBlock(player,0,320,107,107,2,2,FIXE,4,pnum_lvl);
		creerBlock(player,0,427,107,106,1,1,FIXE,5,pnum_lvl);
    creerBlock(player,0,427,107,106,0,0,SOL,39,pnum_lvl);
		creerBlock(player,0,533,107,107,0,0,SOL,6,pnum_lvl);

		creerBlock(player,107,0,106,107,2,2,FIXE,7,pnum_lvl);
		creerBlock(player,107,107,106,106,2,2,FRIABLE,8,pnum_lvl);
		creerBlock(player,107,213,106,107,3,3,SLIME,9,pnum_lvl);
		creerBlock(player,107,320,106,107,2,2,FIXE,11,pnum_lvl);
		creerBlock(player,107,427,106,106,2,1,FIXE,13,pnum_lvl);
		creerBlock(player,107,533,106,107,0,0,SOL,14,pnum_lvl);
		creerBlock(player,107,533,106,107,1,1,NORMAL,15,pnum_lvl);

		creerBlock(player,213,0,107,107,2,2,FIXE,16,pnum_lvl);
		creerBlock(player,213,107,107,106,0,0,SOL,17,pnum_lvl);
		creerBlock(player,213,213,107,107,1,1,NORMAL,18,pnum_lvl);
		creerBlock(player,213,320,107,107,0,0,SOL,19,pnum_lvl);
		creerBlock(player,213,427,107,106,2,1,FIXE,21,pnum_lvl);
		creerBlock(player,213,533,107,107,0,0,SOL,22,pnum_lvl);

		creerBlock(player,320,0,107,107,2,2,FIXE,23,pnum_lvl);
		creerBlock(player,320,107,107,106,2,2,FIXE,24,pnum_lvl);
		creerBlock(player,320,213,107,107,0,0,SOL,25,pnum_lvl);
		creerBlock(player,320,320,107,107,1,1,FIXE,26,pnum_lvl);
		creerBlock(player,320,427,107,106,2,2,SLIME,27,pnum_lvl);
		creerBlock(player,320,533,107,107,0,0,SOL,28,pnum_lvl);

		creerBlock(player,427,0,106,107,2,2,FIXE,29,pnum_lvl);
		creerBlock(player,427,107,106,106,1,1,FRIABLE,30,pnum_lvl);
		creerBlock(player,427,213,106,107,0,0,SOL,31,pnum_lvl);
		creerBlock(player,427,320,106,107,0,0,SOL,32,pnum_lvl);
		creerBlock(player,427,427,106,106,1,1,FIXE,33,pnum_lvl);
		creerBlock(player,427,533,106,107,1,1,SLIME,34,pnum_lvl);

		creerBlock(player,533,0,107,107,2,2,SLIME,35,pnum_lvl);
		creerBlock(player,533,107,107,106,1,1,FIXE,36,pnum_lvl);
		creerBlock(player,533,213,107,107,0,0,FRIABLE,37,pnum_lvl);
		creerBlock(player,533,320,107,107,0,0,SOL,10,pnum_lvl);
		creerBlock(player,533,427,107,106,1,1,FIXE,12,pnum_lvl);
		creerBlock(player,533,533,107,107,0,0,SOL,20,pnum_lvl);

		winBlock(player,0,213,107,107,0,0,1,pnum_lvl);
	}
}


void level5(int * pnum_lvl)
{
  Element * player = initPlayer(0, 0, 62, 74, "./assets/sprite.png");

  if(player)
  {
      creerBlock(player, 80, 80, 80, 80, 1, 1, SLIME, 0, pnum_lvl);
      creerBlock(player, 160, 80, 80, 80, 2, 2, SLIME, 1, pnum_lvl);
      creerBlock(player, 240, 80, 80, 80, 3, 3, SLIME, 2, pnum_lvl);
      creerBlock(player, 320, 80, 80, 80, 3, 3, FIXE, 3, pnum_lvl);
      creerBlock(player, 320, 0, 80, 80, 3, 3, FIXE, 4, pnum_lvl);
      creerBlock(player, 320, 160, 80, 80, 4, 4, NORMAL, 5, pnum_lvl);
      creerBlock(player, 320, 240, 80, 80, 2, -1, NORMAL, 6, pnum_lvl);
      creerBlock(player, 320, 320, 80, 80, 4, 4, NORMAL, 7, pnum_lvl);
      creerBlock(player, 320, 400, 80, 80, 3, 3, FIXE, 8, pnum_lvl);
      creerBlock(player, 240, 400, 80, 80, 2, 2, FRIABLE, 9, pnum_lvl);
      creerBlock(player, 160, 400, 80, 80, 1, 1, FRIABLE, 10, pnum_lvl);
      creerBlock(player, 80, 400, 80, 80, 0, 0, FRIABLE, 11, pnum_lvl);
      creerBlock(player, 0, 240, 320, 80, 1, 0, FIXE, 12, pnum_lvl);
      creerBlock(player, 400, 240, 320, 80, 1, 0, FIXE, 13, pnum_lvl);



      //SOL

      creerBlock(player, 0, 0, 320, 80, 0, 0, SOL, 14, pnum_lvl);
      creerBlock(player, 400, 0, 240, 240, 0, 0, SOL, 15, pnum_lvl);
      creerBlock(player, 0, 80, 80, 80, 0, 0, SOL, 16, pnum_lvl);
      creerBlock(player, 0, 160, 320, 80, 0, 0, SOL, 17, pnum_lvl);
      creerBlock(player, 0, 320, 320, 80, 0, 0, SOL, 18, pnum_lvl);
      creerBlock(player, 0, 400, 80, 80, 0, 0, SOL, 19, pnum_lvl);
      creerBlock(player, 0, 480, 400, 160, 0, 0, SOL, 20, pnum_lvl);
      creerBlock(player, 400, 320, 240, 320, 0, 0, SOL, 21, pnum_lvl);

      winBlock(player,240,560,80,80,0,0,22,pnum_lvl);
  }
}
