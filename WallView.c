#include "WallView.h"
#include "Block.h"
#include "perso.h"
#include "donnees.h"
#include <stdlib.h>
#include <SANDAL2/SANDAL2.h>

int QuelMur(Element * Perso) {
  int valeur = -1;
  if(Perso) {
    if(Perso->x <= 0 + MARGE) {
      valeur = WALL_LEFT;
    }
    else if(Perso->x + Perso->width >= LFN - MARGE) {
      valeur = WALL_RIGHT;
    }
    else if(Perso->y <= 0 + MARGE) {
      valeur = WALL_BACK;
    }
    else if(Perso->y + Perso->height >= HFN - MARGE) {
      valeur = WALL_FRONT;
    }
  }
  return valeur;
}

void AffichePerso(Element * Perso, int valeurMur)
{
  Perso_t * d = NULL;
  float x = 0.0, y = 0.0, largeur = 100.0, hauteur = HFN/7;
  Element * p;
  if(Perso) {
    d = Perso->data;
    if(d) {
      y = (5 - d->level)*hauteur;
      switch (valeurMur) {
        case WALL_LEFT:
        x = Perso->y;
        break;
        case WALL_BACK:
        x = LFN-(Perso->x+Perso->width);
        break;
        case WALL_RIGHT:
        x = HFN-(Perso->y+Perso->height);
        break;
        case WALL_FRONT:
        x = Perso->x;
        break;
      }
      p = createImage(x,y,largeur,hauteur,"assets/perso.png",MUR_d,PERSO_p);
      setKeyPressedElement(p,EndViewWall);
    }
  }
}

void AfficheBlock(Element * Block, int valeurMur, int * idBloc, int nbBloc)
{
  //int white[4] = {255,255,255,255};
  DataBlock * d = NULL;
  float x = 0.0, y = 0.0, largeur = 0.0, hauteur = HFN/7;
  int i = 0;
  if(Block && idBloc) {
    d = Block->data;
    if(d) {
      while(i != nbBloc && idBloc[i] != d->id) {
        i++;
      }
      if(i != nbBloc) {
        y = (6 - d->level)*hauteur;
        switch (valeurMur) {
          case WALL_LEFT:
          x = Block->y;
          largeur = Block->height;
          break;
          case WALL_BACK:
          x = LFN-(Block->x+Block->width);
          largeur = Block->width;
          break;
          case WALL_RIGHT:
          x = HFN-(Block->y+Block->height);
          largeur = Block->height;
          break;
          case WALL_FRONT:
          x = Block->x;
          largeur = Block->width;
          break;
        }
        switch (d->type) {
          case SOL:
          createImage(x,y,largeur,hauteur*(d->level+1-d->level_bas),"assets/b7.png",MUR_d,BLOCK_p+i);
          break;
          case FIXE:
          createImage(x,y,largeur,hauteur*(d->level+1-d->level_bas),"assets/b1.png",MUR_d,BLOCK_p+i);
          break;
          case SLIME:
          createImage(x,y,largeur,hauteur*(d->level+1-d->level_bas),"assets/b2.png",MUR_d,BLOCK_p+i);
          break;
          case FRIABLE:
          createImage(x,y,largeur,hauteur*(d->level+1-d->level_bas),"assets/b6.png",MUR_d,BLOCK_p+i);
          break;
          case WIN:
          createImage(x,y,largeur,hauteur*(d->level+1-d->level_bas),"assets/b5.png",MUR_d,BLOCK_p+i);
          break;
          case NORMAL:
          createImage(x,y,largeur,hauteur*(d->level+1-d->level_bas),"assets/b3.png",MUR_d,BLOCK_p+i);
          break;
        }
      }
    }
  }
}

float * TrieBlock(Element * Perso, int valeurMur) {
  float * tab = NULL;
  Element * bloc = NULL;
  DataBlock * db = NULL;
  Perso_t * d = NULL;
  int i = 0;
  if(Perso) {
    d = Perso->data;
    if(d) {
      tab = malloc(d->nbBloc*sizeof(float));
      if(tab) {
        initIteratorElement(Perso);
        bloc = nextIteratorElement(Perso);
        while(bloc) {
          db = bloc->data;
          if(db) {
            switch (valeurMur) {
              case WALL_LEFT:
              tab[i] = bloc->x;
              break;
              case WALL_BACK:
              tab[i] = bloc->y;
              break;
              case WALL_FRONT:
              tab[i] = HFN-(bloc->y+bloc->height);
              break;
              case WALL_RIGHT:
              tab[i] = LFN-(bloc->x+bloc->width);
              break;
            }
            i++;
            bloc = nextIteratorElement(Perso);
          }
        }
      }
    }
  }
  return tab;
}

void ViewWall(Element * Perso) {
  Element * bloc = NULL;
  int valeurMur = 0;
  float * tab = NULL;
  int * idBloc = NULL;
  Perso_t * d = NULL;
  if(Perso) {
    d = Perso->data;
    if(d) {
      valeurMur = QuelMur(Perso);
      //printf("%d\n", valeurMur );
      if(valeurMur != -1) {
        setDisplayCodeWindow(MUR_d);
        tab = TrieBlock(Perso, valeurMur);
        if(tab) {
          idBloc = TrieTab(tab,d->nbBloc);
          AffichePerso(Perso,valeurMur);
          initIteratorElement(Perso);
          bloc = nextIteratorElement(Perso);
          while(bloc) {
            AfficheBlock(bloc,valeurMur,idBloc,d->nbBloc);
            bloc = nextIteratorElement(Perso);
          }
        }
      }
    }
  }
}

void EndViewWall(Element * Perso, SDL_Keycode c) {


  if(Perso && c == 27) {
    //printf("%d\n",c);
    setDisplayCodeWindow(JEU_d);
    clearDisplayCode(MUR_d);
  }
}
