#ifndef __BLOCK_H__
#define __BLOCK_H__

#define RANGE_ACTION 80

#include <SANDAL2/SANDAL2.h>
#include "levels.h"

enum {
  SOL = 0,
  FIXE = 1,
  SLIME = 2,
  FRIABLE = 3,
  NORMAL = 4,
  WIN = -1
};

typedef struct DataBlock {
  int id;
  int level;
  int type;
  Element * player;
  int smashed;
  int pumped;
  int level_bas;
  int * num_lvl;
} DataBlock;


Element * winBlock(Element * play, int x, int y, int width, int height, int lvl, int lvl_bas, int id, int * pnum_lvl);
void win_condition(Element * block, int click);
Element * creerBlock(Element * play, int x, int y, int width, int height, int lvl, int lvl_bas, int typ, int id, int * pnum_lvl);
void flatten(Element * player,Element * block);
void heigthen(Element * player,Element * block);
void click(Element * block,int click);

DataBlock * initDataBlock(Element * play, int level, int level_bas, int type, int id, int * pnum_lvl);
int * TrieTab(float * tab, int nbBloc);


#endif
