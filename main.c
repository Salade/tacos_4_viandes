#include <SANDAL2/SANDAL2.h>
#include <SDL2/SDL_mixer.h>
#include <stdio.h>
#include "perso.h"
#include "musicV2.h"
#include "donnees.h"
#include "Block.h"
#include "WallView.h"

int main()
{
  void (*t_lvl[MAX_LVL])(int * pnum_lvl) = {level1,level2, level5, level4,level,levelx};

  int pnum_lvl = 0;

  int black[] = {0,0,0,255};

  ZIK * mus = NULL;

  if(initAllSANDAL2(IMG_INIT_PNG))
  {
    puts("Failed to init SANDAL2");
    return -1;
  }

  SDL_Init(SDL_INIT_AUDIO);
  Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT,2,2048);

    if(createWindow(LFN, HFN,"Fenêtre",0,black,JEU_d))
  {
    puts("Failed to open the window");
    closeAllSANDAL2();
  }

  mus = initZIK("liste_musique");
  go_music(mus, 1);

  t_lvl[0](&pnum_lvl);

  while(!PollEvent(NULL))
  {
    updateWindow();
    displayWindow();
    SDL_Delay(16);
  }

  closeAllWindow();
  closeAllSANDAL2();


  return 0;
}
