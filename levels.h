#ifndef LEVELS_H
#define LEVELS_H

#include <SANDAL2/SANDAL2.h>
#include "donnees.h"

void level1(int * pnum_lvl);
void level2(int * pnum_lvl);
void level4(int * pnum_lvl);
void level(int * pnum_lvl);
void levelx(int * pnum_lvl);
void level5(int * pnum_lvl);

#endif
