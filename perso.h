#ifndef PERSO_H
#define PERSO_H

#include <SANDAL2/SANDAL2.h>

enum {
  WAIT, MVT, HAMMER, PUMP
};

typedef struct perso{

  int up;
  int left;
  int down;
  int right;
  int speed;
  int level;
  int nbBloc;

  int xcol;
  int ycol;
  int wcol;
  int hcol;

  int plan;
  int verif;

}Perso_t;


Element * initPlayer(int x, int y, int largeur, int hauteur, char * sprite);

void endJump(Element *this,int code);

Perso_t * initDataPerso(int speed);

void key_press_nograv(Element * player, SDL_Keycode c);

void key_release_nograv(Element * player, SDL_Keycode c);

void movePlayerNograv(Element * player);

#endif
