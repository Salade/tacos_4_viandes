#include "Block.h"
#include "donnees.h"
#include <stdlib.h>
#include "perso.h"
#include "levels.h"


Element * winBlock(Element * play, int x, int y, int width, int height, int lvl, int lvl_bas, int id, int * pnum_lvl)
{
    Element * win = createImage(x,y,width,height,"assets/b5.png",JEU_d,BLOCK_p - lvl + 1);
    Perso_t * d = NULL;

    if (win)
    {
        DataBlock * p = initDataBlock(play,lvl, lvl_bas, WIN, id, pnum_lvl);
        setDataElement(win,(void *)p);
        addElementToElement(play,win);
        addClickableElement(win,rectangleClickable(0.f,0.f,1.f,1.f),0);
        setOnClickElement(win,win_condition);

        d = play->data;

        if(d) {
            d->nbBloc ++;
        }
    }

    return win;
}

void win_condition(Element * block, int click)
{
    void (*t_lvl[MAX_LVL])(int * pnum_lvl) = {level1,level2, level5, level4,level,levelx};
    if (click == 1)
    {
        //printf("tap\n");
        DataBlock * d_block;
        Perso_t * struct_perso;
        getDataElement(block,(void **) &d_block);
        getDataElement(d_block->player,(void **)&struct_perso);

        int inRange = 0;
        if((d_block->player->x) + (struct_perso->xcol) + (struct_perso->wcol) < (block->x))
        {
            if((d_block->player->y) + (struct_perso->ycol) > (block->y) - RANGE_ACTION && (d_block->player->y) + (struct_perso->ycol) + (struct_perso->hcol) < (block->y) + (block->height) + RANGE_ACTION)
                inRange = abs(((block->x) - (struct_perso->xcol) - (d_block->player->x) - (struct_perso->wcol))) < RANGE_ACTION;
        }
        else if((struct_perso->xcol) + (d_block->player->x) > (block->x) + (block->width))
        {
            if((d_block->player->y) + (struct_perso->ycol) > (block->y) - RANGE_ACTION && (d_block->player->y) + (struct_perso->ycol) + (struct_perso->hcol) < (block->y) + (block->height) + RANGE_ACTION)
                inRange = abs(((struct_perso->xcol) + (d_block->player->x) - (block->x) - (block->width))) < RANGE_ACTION;
        }
        else if((struct_perso->ycol) + (d_block->player->y) + (struct_perso->hcol) < (block->y))
        {
            if((d_block->player->x) + (struct_perso->xcol) > (block->x) - RANGE_ACTION && (d_block->player->x) + (struct_perso->xcol) + (struct_perso->wcol) < (block->x) + (block->width) + RANGE_ACTION)
                inRange = abs(((block->y) - (struct_perso->ycol) - (d_block->player->y) - (struct_perso->ycol))) < RANGE_ACTION;
        }
        else
        {
            if((d_block->player->x) + (struct_perso->xcol) > (block->x) - RANGE_ACTION && (d_block->player->x) + (struct_perso->xcol) + (struct_perso->wcol) < (block->x) + (block->width) + RANGE_ACTION)
                inRange = abs(((struct_perso->ycol) + (d_block->player->y) - (block->y) - (block->height))) < RANGE_ACTION;
        }

        if (inRange && struct_perso->level == d_block->level)
            {
                clearDisplayCode(JEU_d);
                (*d_block->num_lvl) += 1;

                if (*d_block->num_lvl < MAX_LVL)
                    t_lvl[*d_block->num_lvl](d_block->num_lvl);
                else
                {
                    closeAllWindow();
                    closeAllSANDAL2();
                }
            }
    }
}

Element * creerBlock(Element * play, int x, int y, int width, int height, int lvl, int lvl_bas, int typ, int id, int * pnum_lvl)
{
    Element * e = NULL;

    switch (typ)
    {
        case SOL :
            e = createImage(x,y,width,height,"assets/b7.png",JEU_d,BLOCK_p - lvl + 1);
            break;
        case FIXE :
            e = createImage(x,y,width,height,"assets/b1.png",JEU_d,BLOCK_p - lvl + 1);
            break;
        case SLIME :
            e = createImage(x,y,width,height,"assets/b2.png",JEU_d,BLOCK_p - lvl + 1);
            break;
        case FRIABLE :
            e = createImage(x,y,width,height,"assets/b6.png",JEU_d,BLOCK_p - lvl + 1);
            break;
        case NORMAL :
          e = createImage(x,y,width,height,"assets/b3.png",JEU_d,BLOCK_p - lvl + 1);
          break;
	   }

    Perso_t * d = NULL;
    if (e)
	{
		DataBlock * p = initDataBlock(play,lvl, lvl_bas, typ, id, pnum_lvl + 1);
		setDataElement(e,(void *)p);
		addElementToElement(play,e);
		if(typ > 1)
		{
			addClickableElement(e,rectangleClickable(0.f,0.f,1.f,1.f),0);
			setOnClickElement(e,click);
		}

		d = play->data;
		if(d) {
			d->nbBloc ++;
		}
	}

	return e;
}

void flatten(Element * player,Element * block)
{
    Perso_t * struct_perso;
    DataBlock * d_block = malloc(sizeof(DataBlock));
    getDataElement(block,(void **)&d_block);
    getDataElement(player,(void **)&struct_perso);

    int inRange = 0;
    int estPasDedans = (((struct_perso->ycol + player->y)+(struct_perso->hcol)-(block->y)< 0) || ((struct_perso->ycol + player->y)-(block->y)-(block->height)> 0) || ((struct_perso->xcol + player->x)+(struct_perso->wcol)-(block->x)< 0) || ((struct_perso->xcol + player->x)-(block->x)-(block->width)> 0));

    if((player->x) + (struct_perso->xcol) + (struct_perso->wcol) < (block->x))
    {
        if((player->y) + (struct_perso->ycol) > (block->y) - RANGE_ACTION && (player->y) + (struct_perso->ycol) + (struct_perso->hcol) < (block->y) + (block->height) + RANGE_ACTION)
            inRange = abs(((block->x) - (struct_perso->xcol) - (player->x) - (struct_perso->wcol))) < RANGE_ACTION;
    }
    else if((struct_perso->xcol) + (player->x) > (block->x) + (block->width))
    {
        if((player->y) + (struct_perso->ycol) > (block->y) - RANGE_ACTION && (player->y) + (struct_perso->ycol) + (struct_perso->hcol) < (block->y) + (block->height) + RANGE_ACTION)
            inRange = abs(((struct_perso->xcol) + (player->x) - (block->x) - (block->width))) < RANGE_ACTION;
    }
    else if((struct_perso->ycol) + (player->y) + (struct_perso->hcol) < (block->y))
    {
        if((player->x) + (struct_perso->xcol) > (block->x) - RANGE_ACTION && (player->x) + (struct_perso->xcol) + (struct_perso->wcol) < (block->x) + (block->width) + RANGE_ACTION)
            inRange = abs(((block->y) - (struct_perso->ycol) - (player->y) - (struct_perso->ycol))) < RANGE_ACTION;
    }
    else
    {
        if((player->x) + (struct_perso->xcol) > (block->x) - RANGE_ACTION && (player->x) + (struct_perso->xcol) + (struct_perso->wcol) < (block->x) + (block->width) + RANGE_ACTION)
            inRange = abs(((struct_perso->ycol) + (player->y) - (block->y) - (block->height))) < RANGE_ACTION;
    }

    if(inRange && estPasDedans && d_block->type != FRIABLE)
    {
    	if(struct_perso->level == d_block->level || struct_perso->level == (d_block->level)-1)
    	{
        if(d_block->type != SLIME)
        {
          d_block->level -= 1;
          d_block->level_bas -= 1;
          setPlanElement(block, JEU_d, BLOCK_p - d_block->level + 1);
        }
        else
        {
           if(!d_block->smashed)
           {
             d_block->level -= 1;
             d_block->level_bas -= 1;
             setPlanElement(block, JEU_d, BLOCK_p - d_block->level + 1);
           }
        }

        d_block->smashed = 1;
        d_block->pumped = 0;
        setAnimationElement(player,HAMMER);
    	}
    }
}

void heigthen(Element * player,Element * block)
{
    Perso_t * struct_perso;
    DataBlock * d_block;
    getDataElement(block,(void **)&d_block);
    getDataElement(player,(void **)&struct_perso);

    int inRange = 0;
    int estPasDedans = (((struct_perso->ycol + player->y)+(struct_perso->hcol)-(block->y)< 0) || ((struct_perso->ycol + player->y)-(block->y)-(block->height)> 0) || ((struct_perso->xcol + player->x)+(struct_perso->wcol)-(block->x)< 0) || ((struct_perso->xcol + player->x)-(block->x)-(block->width)> 0));

    if((player->x) + (struct_perso->xcol) + (struct_perso->wcol) < (block->x))
    {
        if((player->y) + (struct_perso->ycol) > (block->y) - RANGE_ACTION && (player->y) + (struct_perso->ycol) + (struct_perso->hcol) < (block->y) + (block->height) + RANGE_ACTION)
            inRange = abs(((block->x) - (struct_perso->xcol) - (player->x) - (struct_perso->wcol))) < RANGE_ACTION;
    }
    else if((struct_perso->xcol) + (player->x) > (block->x) + (block->width))
    {
        if((player->y) + (struct_perso->ycol) > (block->y) - RANGE_ACTION && (player->y) + (struct_perso->ycol) + (struct_perso->hcol) < (block->y) + (block->height) + RANGE_ACTION)
            inRange = abs(((struct_perso->xcol) + (player->x) - (block->x) - (block->width))) < RANGE_ACTION;
    }
    else if((struct_perso->ycol) + (player->y) + (struct_perso->hcol) < (block->y))
    {
        if((player->x) + (struct_perso->xcol) > (block->x) - RANGE_ACTION && (player->x) + (struct_perso->xcol) + (struct_perso->wcol) < (block->x) + (block->width) + RANGE_ACTION)
            inRange = abs(((block->y) - (struct_perso->ycol) - (player->y) - (struct_perso->ycol))) < RANGE_ACTION;
    }
    else
    {
        if((player->x) + (struct_perso->xcol) > (block->x) - RANGE_ACTION && (player->x) + (struct_perso->xcol) + (struct_perso->wcol) < (block->x) + (block->width) + RANGE_ACTION)
            inRange = abs(((struct_perso->ycol) + (player->y) - (block->y) - (block->height))) < RANGE_ACTION;
    }

    if(inRange && estPasDedans && d_block->type != SLIME)
    {
    	if(struct_perso->level == d_block->level || struct_perso->level == (d_block->level)+1)
    	{
        if(d_block->type != FRIABLE)
        {
          d_block->level += 1;
          d_block->level_bas += 1;
          setPlanElement(block, JEU_d, BLOCK_p - d_block->level + 1);
        }
        else
        {
          if(!d_block->pumped)
          {
            d_block->level += 1;
            d_block->level_bas += 1;
            setPlanElement(block, JEU_d, BLOCK_p - d_block->level + 1);
          }
        }
        d_block->smashed = 0;
        d_block->pumped = 1;
        setAnimationElement(player,PUMP);
    	}
    }
}

DataBlock * initDataBlock(Element * play,int level, int level_bas, int type, int id, int * pnum_lvl) {
    DataBlock * d = malloc(sizeof(DataBlock));
    if(d) {
        d->id = id;
        d->level = level;
        d->level_bas = level_bas;
        d->type = type;
        if (type == -1)
            d->num_lvl = pnum_lvl;
        d->player = play;
        d->smashed = 0;
        d->pumped = 0;
    }
    return d;
}

void click(Element * block,int clk)
{
    DataBlock * d_block = NULL;
    if(block) {
      d_block = block->data;
      if(d_block) {
        if(clk == 1)
        {
        	flatten(d_block->player,block);
        }
        else
        {
        	if(clk == 3)
        	{
        		heigthen(d_block->player,block);
        	}
        }
      }
    }
}

int * TrieTab(float * tab, int nbBloc)
{
	int * tabid = NULL;
	int i;
	int j;
	float a;
	int b;
	if(tab)
	{
		tabid = malloc(nbBloc * sizeof(int));
		if(tabid) {
			for(i = 0; i < nbBloc; i ++) {
				tabid[i] = i;
			}
			for(i=0;i<nbBloc-1;i++)
			{
				for(j=0;j<nbBloc-1-i;j++)
				{
					if(tab[j] > tab[j+1])
					{
						a = tab[j];
						tab[j] = tab[j+1];
						tab[j+1] = a;
						b = tabid[j];
						tabid[j] = tabid[j+1];
						tabid[j+1] = b;
					}
				}
			}
		}
	}

	return tabid;
}
