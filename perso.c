#include "perso.h"
#include "Block.h"
#include "donnees.h"
#include "WallView.h"

Element * initPlayer(int x, int y, int largeur, int hauteur, char * sprite){

  Element * player = createImage(x,y,largeur,hauteur,sprite,JEU_d,PERSO_p);
  Perso_t * structPerso = initDataPerso(3);

  int hauteur_sprite = 74, largeur_sprite  = 62;

  if(player && structPerso)
  {
    setDataElement(player,(void*)structPerso);

    setKeyPressedElement(player,key_press_nograv);
    setKeyReleasedElement(player,key_release_nograv);
    setActionElement(player,movePlayerNograv);

    //animation de wait

    addAnimationElement(player,WAIT);
    addSpriteAnimationElement(player, WAIT, 0, 0,largeur_sprite ,hauteur_sprite ,5,0);
    setWaySpriteAnimationElement(player,WAIT,1);

    //animation de marche
    addAnimationElement(player,MVT);
    addSpriteAnimationElement(player, MVT, 3 * largeur_sprite, 0,largeur_sprite ,hauteur_sprite ,5,0);
    addSpriteAnimationElement(player, MVT, 4 * largeur_sprite, 0,largeur_sprite ,hauteur_sprite ,5,1);
    setWaySpriteAnimationElement(player,MVT,1);

    addAnimationElement(player,HAMMER);
    addSpriteAnimationElement(player, HAMMER, largeur_sprite, 0,largeur_sprite ,hauteur_sprite ,10,0);
    addSpriteAnimationElement(player, HAMMER, 0, 0,largeur_sprite ,hauteur_sprite , 10, 1);
    addSpriteAnimationElement(player, HAMMER, largeur_sprite, 0,largeur_sprite ,hauteur_sprite , 10, 2);
    addSpriteAnimationElement(player, HAMMER, 0, 0,largeur_sprite ,hauteur_sprite , 10, 3);
    setWaySpriteAnimationElement(player,HAMMER,1);

    addAnimationElement(player,PUMP);
    addSpriteAnimationElement(player, PUMP, 2 * largeur_sprite, 0,largeur_sprite ,hauteur_sprite , 10, 0);
    addSpriteAnimationElement(player, PUMP, 0, 0,largeur_sprite ,hauteur_sprite , 10, 1);
    addSpriteAnimationElement(player, PUMP, 2 * largeur_sprite, 0,largeur_sprite ,hauteur_sprite , 10, 2);
    addSpriteAnimationElement(player, PUMP, 0, 0,largeur_sprite ,hauteur_sprite , 10, 3);
    setWaySpriteAnimationElement(player,PUMP,1);


    setEndSpriteElement(player,endJump);


  }

  return player;

}

void endJump(Element *this,int code){

  if(code == HAMMER || code == PUMP)
  {
    setAnimationElement(this,WAIT);
  }

}

Perso_t * initDataPerso(int speed)
{
  Perso_t * p = malloc(sizeof(Perso_t));

  if(p)
  {
    p->up = 0;
    p->left = 0;
    p->down = 0;
    p->right = 0;
    p->speed = speed;
    p->level = 0;
    p->nbBloc = 0;

    p->xcol = 11;
    p->ycol = 16;
    p->wcol = 40;
    p->hcol = 40;

    p->plan = PERSO_p;
    p->verif = 0;
  }

  return p;
}

void key_press_nograv(Element * player, SDL_Keycode c){

  Perso_t * structPerso;

  getDataElement(player,(void**)&structPerso);

  switch (c) {
    //direction x
    case 'q':
    {
      structPerso->left=1;
      if(!structPerso->verif && structPerso->right) {
        setAnimationElement(player,WAIT);
        structPerso->verif = 1;
      }
      else if (!structPerso->verif) {
        setAnimationElement(player,MVT);
        setAngleElement(player, -90);
      }
      break;
    }
    case 'd':
    {
      structPerso->right=1;
      if(!structPerso->verif && structPerso->left) {
        setAnimationElement(player,WAIT);
        structPerso->verif = 1;
      }
      else if (!structPerso->verif) {
        setAnimationElement(player,MVT);
        setAngleElement(player, 90);
      }
      break;

    }
    //direction y
    case 'z':
    {
      structPerso->up=1;
      if(!structPerso->verif && structPerso->down) {
        setAnimationElement(player,WAIT);
        structPerso->verif = 1;
      }
      else if (!structPerso->verif) {
        setAnimationElement(player,MVT);
        setAngleElement(player, 0);
      }

      break;
    }
    case 's':
    {
      structPerso->down=1;
      if(!structPerso->verif && structPerso->up) {
        setAnimationElement(player,WAIT);
        structPerso->verif = 1;
      }
      else if (!structPerso->verif) {
        setAnimationElement(player,MVT);
        setAngleElement(player, -180);
      }

      break;
    }
  }
}

void key_release_nograv(Element * player, SDL_Keycode c){

  Perso_t * structPerso;

  getDataElement(player,(void**)&structPerso);

  switch (c) {
    //direction x
    case 'q':
    {
      if(structPerso->right) {
        structPerso->verif = 0;
        setAngleElement(player, 90);
        setAnimationElement(player,MVT);
      }
      structPerso->left=0;
      break;
    }
    case 'd':
    {
      if(structPerso->left) {
        structPerso->verif = 0;
        setAngleElement(player, -90);
        setAnimationElement(player,MVT);
      }
      structPerso->right=0;
      break;
    }
    //direction y
    case 'z':
    {
      if(structPerso->down) {
        structPerso->verif = 0;
        setAngleElement(player, -180);
        setAnimationElement(player,MVT);
      }
      structPerso->up=0;
      break;
    }
    case 's':
    {
      if(structPerso->up) {
        structPerso->verif = 0;
        setAngleElement(player, 0);
        setAnimationElement(player,MVT);
      }
      structPerso->down=0;
      break;
    }
  }

  if(!structPerso->up && !structPerso->down && !structPerso->left && !structPerso->right)
  {
    setAnimationElement(player,WAIT);
  }
}

void movePlayerNograv(Element * player){

  Perso_t * structPerso;

  DataBlock * structBlock;

  float xplayer, yplayer, wplayer, hplayer, x, y;
  float xwall, ywall, wwall, hwall;
  Element * cour;
  int collision = 0;

  getDataElement(player,(void**)&structPerso);

  getCoordElement(player,&x,&y);

  xplayer = x + structPerso->xcol;
  yplayer = y + structPerso->ycol;
  wplayer = structPerso->wcol;
  hplayer = structPerso->hcol;

  initIteratorElement(player);

  while((cour = nextIteratorElement(player)))
  {

    getCoordElement(cour,&xwall,&ywall);
    getDimensionElement(cour,&wwall,&hwall);
    getDataElement(cour, (void **)&structBlock);

    if(structPerso->level != structBlock->level && structPerso->level > structBlock->level_bas - 2)
    {

      collision = (xplayer + wplayer + structPerso->speed*(structPerso->right-structPerso->left) >= xwall && xplayer + structPerso->speed*(structPerso->right-structPerso->left) <= xwall + wwall && yplayer + hplayer + structPerso->speed*(structPerso->down-structPerso->up) >= ywall && yplayer + structPerso->speed*(structPerso->down-structPerso->up) <= ywall + hwall);

      if(collision)
      {

        if(yplayer + hplayer < ywall)
        {
          structPerso->down=0;
        }
        if(xplayer + wplayer < xwall)
        {
          structPerso->right=0;
        }
        if(xplayer > xwall + wwall)
        {
          structPerso->left=0;
        }
        if(yplayer > ywall + hwall)
        {
          structPerso->up=0;
        }

      }
    }
    else
    {
      collision = (xplayer + wplayer >= xwall && xplayer <= xwall + wwall && yplayer + hplayer >= ywall && yplayer <= ywall + hwall);

      if(collision)
      {
        if(structBlock->type == SLIME && structBlock->smashed)
        {
          structPerso->level++;
          structBlock->level++;
          structBlock->level_bas ++;
          structBlock->smashed = 0;
          structPerso->plan --;
          setPlanElement(player, JEU_d, structPerso->plan);
        }
        else if(structBlock->type == FRIABLE && structBlock->pumped)
        {
          structPerso->level--;
          structBlock->level--;
          structBlock->level_bas --;
          structBlock->pumped = 0;
          structPerso->plan ++;
          setPlanElement(player, JEU_d, structPerso->plan);

        }
      }
    }
    //printf("%d, %d\n", structPerso->level, structBlock->level);

  }

  //test si sortie d'écran
  collision = (xplayer + wplayer + structPerso->speed*(structPerso->right-structPerso->left) >= LFN || xplayer + structPerso->speed*(structPerso->right-structPerso->left) <= 0 || yplayer + hplayer + structPerso->speed*(structPerso->down-structPerso->up) >= HFN || yplayer + structPerso->speed*(structPerso->down-structPerso->up) <= 0);

  if(!collision)
  {
    moveElement(player,structPerso->speed*(structPerso->right-structPerso->left),structPerso->speed*(structPerso->down-structPerso->up));
  }
  else
  {
    structPerso->down=0;
    structPerso->right=0;
    structPerso->left=0;
    structPerso->up=0;
    ViewWall(player);
  }

}
