#ifndef __DONNEES_H__
#define __DONNEES_H__

#define LFN 640
#define HFN 640
#define MAX_LVL 6

enum {
  ECRAN_TITRE_d = 0,
  JEU_d = 1,
  MUR_d = 2
};

enum {
  PERSO_p = 0,
  BLOCK_p = 1,

  FOND_p = 100
};

#endif
